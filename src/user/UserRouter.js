const express = require('express');
const UserService = require('./UserService');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const pagination = require('../middleware/pagination');
const UserNotFoundException = require('./UserNotFoundException');
const ForbiddenException = require('../auth/ForbiddenException');
const NotFoundException = require('../error/NotFoundException');
const ValidationException = require('../error/ValidationException');

// const validateUsername = (req, res, next) => {
//   const user = req.body;
//   if (user.username === null) {
//     req.validationErrors = {
//       ...req.validationErrors,
//       username: 'Username cannot be null',
//     };
//   }
//   next();
// };

// const validateEmail = (req, res, next) => {
//   const user = req.body;
//   if (user.email === null) {
//     req.validationErrors = {
//       ...req.validationErrors,
//       email: 'E-mail cannot be null',
//     };
//   }
//   next();
// };

router.post(
  '/api/1.0/users',
  check('username')
    .notEmpty()
    .withMessage('username_null')
    .bail()
    .isLength({ min: 4, max: 32 })
    .withMessage('username_size'),
  check('email')
    .notEmpty()
    .withMessage('email_null')
    .bail()
    .isEmail()
    .withMessage('email_invalid')
    .bail()
    .custom(async (email) => {
      const user = await UserService.findByEmail(email);
      if (user) {
        throw new Error('email_inuse');
      }
    }),
  check('password')
    .notEmpty()
    .withMessage('password_null')
    .bail()
    .isLength({ min: 6 })
    .withMessage('password_size')
    .bail()
    .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).*$/)
    .withMessage('password_pattern'),
  async (req, res) => {
    // if (req.validationErrors) {
    //   const response = { validationErrors: { ...req.validationErrors } };
    //   return res.status(400).send(response);
    // }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const validationErrors = {};
      errors
        .array()
        .forEach((error) => (validationErrors[error.param] = req.t(error.msg)));
      return res.status(400).send({ validationErrors: validationErrors });
    }

    try {
      await UserService.save(req.body);
      return res.send({ message: 'User created' });
    } catch (err) {
      return res.status(502).send();
    }
  }
);

router.post('/api/1.0/users/token/:token', async (req, res) => {
  const token = req.params.token;
  try {
    await UserService.activate(token);
  } catch (err) {
    return res.status(400).send({ message: req.t(err.message) });
  }
  res.send({ message: req.t('account_activation_success') });
});

router.get('/api/1.0/users', pagination, async (req, res) => {
  const { page, size } = req.pagination;
  const users = await UserService.getUsers(page, size);
  res.send(users);
});

router.get('/api/1.0/users/:id', async (req, res, next) => {
  // try {
  //   const user = await UserService.getUser(req.params.id);
  //   res.send(user);
  // } catch (err) {
  //   next(err);
  // }

  try {
    //const user = await UserService.getUser(req.params.id);
    //res.send(user);
    throw new UserNotFoundException();
  } catch (err) {
    next(err);
  }
});

router.put('/api/1.0/users/:id', (req, res) => {
  throw new ForbiddenException('unauthroized_user_update');
});

router.post(
  '/api/1.0/password-reset',
  check('email').isEmail().withMessage('email_invalid'),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return next(new ValidationException(errors.array()));
    }

    const user = await UserService.findByEmail(req.body.email);
    if (user) {
      return res.send('ok');
    }

    return next(new NotFoundException('email_not_inuse'));
  }
);

module.exports = router;
